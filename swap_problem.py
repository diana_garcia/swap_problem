# -*- coding: utf-8 -*-

# Problem:
# Check if a list is in ascending order using at most one swap operation

import unittest

# Function to check if the swap works
def checkSwap(listA, i, j):
    n = len(listA)
    rightI = listA[i+1] >= listA[j]
    leftJ = listA[j-1] <= listA[i]
    leftI = listA[i-1] <= listA[j] if i> 0 else True
    rightJ =  listA[j+1] >= listA[i] if j < n-1 else True
    return (rightI and leftJ and leftI and rightJ)

# Function to perform the swap and check if the list is now
# in ascending order
def swapAndCheckAgain(listA, i, j):
        listA[i], listA[j]  = listA[j], listA[i]
        n = len(listA)
        ordered = True
        for k in xrange(1, n):
            if(listA[k] < listA[k-1]):
                ordered = False
                break
        listA[i], listA[j]  = listA[j], listA[i]
        return ordered

                    
def solution(A):
    oneSwap = False
    lenA = len(A)
    # If the list has less than 2 elements, we'll be able to sort them with 
    # one swap
    if (lenA <= 2):
        oneSwap = True
        
    # I shouldn't have tried to find every misplaced element, one is enough
    # To get the number of misplaced elements, we can compare the size of the
    # longest increasing subsequence with the size of the list.
    else:
        # I < J for the swap
        I = -1
        J = -1
        # If the last element is misplaced, we've found J
        if(A[-1] < A[-2]):
            J = lenA-1
        else:
            prevrepeats = 0
            for k in xrange(1, lenA-1):
                if(A[k-1] > A[k] ):
                    # Check the triplet A[k-1], A[k], A[k+1]
                    # If A[k-1] and A[k+1] form an increasing subsequence, it is 
                    # the extension of a previous incr. sub. (if k > 1) and 
                    # it might be extended further. Mark k as the misplaced index
                    if(A[k+1] > A[k-1]):
                        # A[k] is the smallest number in the triplet because
                        # the swap index (I) must be smaller than k
                        J = k
                    # If A[k-1] and A[k+1] are not part of an increasing subsequence
                    elif(A[k-1] > A[k+1]):
                        # A[k-1] is the largest number in the triplet because
                        # the swap index (J) must be greater than k-1
                        I = k-1
                    else:
                        # A[k] is inside a sequence of repeated elements. If A[k-1]
                        # is the first element of the sequence, select it as the 
                        # first swap index. If it isn't, then A[k] is selected
                        if(prevrepeats == 0):
                            I = k-1
                        else:
                            J = k
                    break
                elif(A[k-1] == A[k]):
                    prevrepeats += 1
                else:
                    prevrepeats = 0
                    
     
        # If there was no misplaced element
        if(I == -1 and J == -1):
            oneSwap = True
        else:
            
            # Find missing J or missing I
            # Binary search could be used but the solution is already O(n)
            nextrepeats = 0
            if(J == -1):
                J = lenA-1
                # Check for larger index values
                # The swap index could be at the beginning or at the end of a 
                # sequence of repeated values
                for k in xrange(I+1, lenA):
                        if(A[k] >= A[I]):
                            J = k-1
                            while(A[k] == A[I] and k < lenA-1):
                                nextrepeats += 1
                                k+= 1
                            break
            elif(I == -1):
                # Check for smaller index values.
                # No need to check for repeated values
                for k in xrange(0, J):
                        if(A[k] > A[J]):
                            I = k
                            break 
                    
            # Check if the swap works
            if(checkSwap(A, I, J)):
                oneSwap = swapAndCheckAgain(A, I, J)
            
            if(not oneSwap and nextrepeats > 0):
                # Undo the swap and check again if a swap with the element 
                # next to the end of the repeated sequence works
                J = J + nextrepeats + 1
                oneSwap = swapAndCheckAgain(A, I, J)                   
    return oneSwap

class swapTests(unittest.TestCase):
    
    def test_empty_list_is_correct(self):
        testList = []
        self.assertTrue(solution(testList))

    def test_two_elements_list_is_correct(self):
        testList = [3,2]
        self.assertTrue(solution(testList))
    
    def test_ascending_list_is_correct(self):
        testList = [2,4,7,8]
        self.assertTrue(solution(testList))
        
    def test_should_swap_last_element(self):
        testList = [0,2,2,2,1]
        self.assertTrue(solution(testList))
    
    ## Also checks swap after repeats
    def test_should_swap_to_larger_index(self):
        testList = [1,5,4,5,5,2,8]
        self.assertTrue(solution(testList))
    
    def test_should_swap_to_smaller_index(self):
        testList = [1,5,5,2,5,5,8]
        self.assertTrue(solution(testList))
    
    def test_should_swap_before_repeats(self):
        testList = [0,7,4,3,7,7,8]
        self.assertTrue(solution(testList))
        

    
        
if __name__ == '__main__':
    unittest.main()
